#include <stdio.h>
#include <string.h>

typedef int bool;
#define true 1
#define false 0

bool DEBUG = false;

void printBoard(int* board, int N) {
	int i;
	for (i = 0; i < N*N; i++) {
		if(i%N == 0) printf("\n|");
		printf("%c|", (*(board + i) ? '#':' '));
	}
	printf("\n\n");
}

int getNumNeighbors(int* board, int N, int* cell) {
	int num = 0;
	int col = (cell-board)%N;
	int row = ((cell-board)/N)*N;
	if (DEBUG) printf("GNN: %d,%d ", col, row);

	if (col > 0) {
		if (*(cell-1)) {num++; if (DEBUG) printf("L ");} //left
		if (row > 0       && *(cell-1-N)) {num++; if (DEBUG) printf("UL ");} //upper left
		if (row < N*(N-1) && *(cell-1+N)) {num++; if (DEBUG) printf("BL ");} //lower left
	}
	if (col < N-1) {
		if (*(cell+1)) {num++; if (DEBUG) printf("R ");}                    //right
		if (row > 0       && *(cell+1-N)) {num++; if (DEBUG) printf("UR ");} //upper right
		if (row < N*(N-1) && *(cell+1+N)) {num++; if (DEBUG) printf("BR ");} //lower right
	}
	if (row < N*(N-1) && *(cell+N)) {num++; if (DEBUG) printf("B ");}       //below
	if (row > 0       && *(cell-N)) {num++; if (DEBUG) printf("U ");}       //above

	if (DEBUG) printf("\n");
	return num;
}

void doStep(int* srcBoard, int* destBoard, int N) {
	int i;
	for (i = 0; i < N*N; i++) {
		int s = getNumNeighbors(srcBoard, N, srcBoard+i);
		if (*(srcBoard+i)) { //Live Cell
			if (s < 2 || s > 3) {
				*(destBoard + i) = 0; //over/under population
			} else {
				*(destBoard + i) = 1; //else survives
			}
		} else if (s == 3) {
			*(destBoard + i) = 1; //reproduction
		} else {
			*(destBoard + i) = 0; //cell stays dead
		}
	}
}

void setupBoard(int* board, int N, int startConf) {
	int startConf1[144] = {
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,1,0,0,0,0,
		0,0,0,1,0,1,0,0,0,
		0,0,1,0,0,0,1,0,0,
		0,0,1,0,0,0,1,0,0,
		0,0,1,0,0,0,1,0,0,
		0,0,1,0,0,0,1,0,0,
		0,0,1,0,0,0,1,0,0,
		0,0,1,0,0,0,1,0,0,
		0,0,0,1,0,1,0,0,0,
		0,0,0,0,1,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0
	};
	int startConf2[9] = {
		0,1,1,
		1,1,0,
		0,1,0
	};
	int startConf3[21] = {
		0,1,0,0,0,0,0,
		0,0,0,1,0,0,0,
		1,1,0,0,1,1,1
	};

	int* selConf;
	int  selConfSize;
	int  selRowNum;
	int  selColNum;
	if (startConf == 1) {
		printf("Pentadecathlon selected\n");
		selConf = startConf1;
		selConfSize = 144;
		selColNum = 9;
		selRowNum = 16;
	} else if (startConf == 3){
		printf("R-pentomino selected\n");
		selConf = startConf3;
		selConfSize = 21;
		selRowNum = 3;
		selColNum = 7;
	} else {
		printf("Acorn selected\n");
		selConf = startConf2;
		selConfSize = 9;
		selColNum = 3;
		selRowNum = 3;
	}
	int i,t;
	for (i = 0; i < N && i < selRowNum; i++) {
		for (t = 0; t < N && t < selColNum; t++) {
			*(board + i*N + t) = *(selConf + i*selColNum + t);
		}
	}
}

void testGetNumNeighbors() {
	int test[9] = {
		1,0,1,
		0,0,0,
		1,0,1
	};

	int numNeighbors[9] = {
		0,2,0,
		2,4,2,
		0,2,0
	};

	int test2[9] = {
		0,1,1,
		1,1,0,
		0,1,0
	};
	int numNeighbors2[9] = {
		3,3,2,
		3,4,4,
		3,2,2
	};
 
	int i;
	bool passed = true;
	for (i = 0; i < 9; i++) {
		int s = getNumNeighbors(test, 3, test+i);
		if (s != *(numNeighbors+i)) {
			printf("Test %d failed expected %d got %d\n\n", i+1, *(numNeighbors+i), s);
			passed = false;
		}
	}
	for (i = 0; i < 9; i++) {
		int s = getNumNeighbors(test2, 3, test2+i);
		if (s != *(numNeighbors2+i)) {
			printf("Test %d failed expected %d got %d\n\n", i+1, *(numNeighbors2+i), s);
			passed = false;
		}
	}
	if (passed) 
		printf("getNumNeighbors working, all tests passed\n");
	

}

void testDoStep() {
	int buffer[9] = {0,0,0,0,0,0,0,0,0};
	int test[9] = {
		1,0,1,
		0,0,0,
		1,0,1
	};
	int testStep[9] = {
		0,0,0,
		0,0,0,
		0,0,0
	};
	int test2[9] = {
		0,1,1,
		1,1,0,
		0,1,0
	};
	int test2Step[9] = {
		1,1,1,
		1,0,0,
		1,1,0
	};
	int test2Step2[9] = {
		1,1,0,
		0,0,1,
		1,1,0
	};
	int test2Step3[9] = {
		0,1,0,
		0,0,1,
		0,1,0
	};

	int passed = true;
	int i;
	
	doStep(test, buffer, 3);
	for (i = 0; i < 9; i++) {
		if (*(testStep+i) != *(buffer+i)) {
			printf("test 1 failed\nexpected:");
			printBoard(testStep, 3);
			printf("got:");
			printBoard(buffer, 3);
			passed = false;
			break;
		}
	}
	doStep(test2, buffer, 3);
	for (i = 0; i < 9; i++) {
		if (*(test2Step+i) != *(buffer+i)) {
			printf("test 2 failed\nexpected:");
			printBoard(test2Step, 3);
			printf("got:");
			printBoard(buffer, 3);
			passed = false;
			break;
		}
	}
	doStep(buffer, test2, 3);
	for (i = 0; i < 9; i++) {
		if (*(test2Step2+i) != *(test2+i)) {
			printf("test 3 failed\nexpected:");
			printBoard(test2Step2, 3);
			printf("got:");
			printBoard(test2, 3);
			passed = false;
			break;
		}
	}
	doStep(test2, buffer, 3);
	for (i = 0; i < 9; i++) {
		if (*(test2Step3+i) != *(buffer+i)) {
			printf("test 4 failed\nexpected:");
			printBoard(test2Step3, 3);
			printf("got:");
			printBoard(buffer, 3);
			passed = false;
			break;
		}
	}
	if (passed) 
		printf("doStep working, all tests passed\n");

}

int main(int argc, char* argv[]) {
	if (argc > 1 && argv[1][0] == '-' && argv[1][1] == 't') {
		testGetNumNeighbors();
		testDoStep();
		return 0;
	} 
	printf("Game of Life by CD Silsby\n");
	printf("Please enter the size of the board\n");
	int N;
	scanf("%d", &N);
	printf("Please select a starting configuration\n");
	printf("1  Pentadecathlon\n");
	printf("2  R-pentomino\n");
	printf("3  Acorn\n");
	int startConf;
	scanf("%d", &startConf);
	int board1[N*N];
	int board2[N*N];
	memset(board1, 0, N*N * sizeof(int));
	memset(board2, 0, N*N * sizeof(int));

	setupBoard(board1, N, startConf);
	printf("Press the ENTER key to continue\n");
	printBoard(board1, N);
	getchar();
	bool swap = true;
	while (getchar() != 'q') {
		if (swap) {
			doStep(board1, board2, N);
			printBoard(board2, N);
		} else {
			doStep(board2, board1, N);
			printBoard(board1, N);
		}
		swap = (swap ? 0 : 1);
	}	
}
