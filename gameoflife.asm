.text 
.globl main

main:
	# $s0 = N
	# $t0 = startConf, char
	# $s1 = swap
	# $s2 = 113 'q'
	li $v0, 4
	la $a0, name
	syscall 
	la $a0, prompt1
	syscall

	li $v0, 5
	syscall
	move $s0, $v0

	li $v0, 4
	la $a0, prompt2
	syscall
	la $a0, option1
	syscall
	la $a0, option2
	syscall
	la $a0, option3
	syscall

	li $v0, 5
	syscall
	move $t0, $v0

	la $a0, board1
	move $a1, $s0
	move $a2, $t0

	jal setupBoard
	li $v0, 4
	la $a0, continue
	syscall

	la $a0, board1
	move $a1, $s0
	jal printBoard

	li $v0, 12
	syscall
	move $t0, $v0
	li $s2, 113 #'q'
	li $s1, 1
	mainLoop:
		beq $s2, $t0, afterMainLoop
		mainIf:
			beqz $s1, mainElse
			la $a0, board1
			la $a1, board2
			move $a2, $s0
			jal doStep
			la $a0, board2
			move $a1, $s0
			jal printBoard
			j mainAfterIf
		mainElse:
			la $a0, board2
			la $a1, board1
			move $a2, $s0
			jal doStep
			la $a0, board1
			move $a1, $s0
			jal printBoard
			j mainAfterIf
		mainAfterIf:

		mainIf2:
			beqz $s1, mainElse2
			li $s1, 0
			j mainAfterIf2
		mainElse2:
			li $s1, 1
		mainAfterIf2:
		li $v0, 12
		syscall
		move $t0, $v0
		j mainLoop
	afterMainLoop:
	exit:
	li $v0, 10
	syscall

printBoard:
	# $t0 = i
	# $t1 = N*N
	# $t3 = board
	move $t3, $a0
	li $t0, 0
	mult $a1, $a1
	mflo $t1
	printBoardLoop:
		beq $t0, $t1, printBoardAfterLoop

		printBoardIf:
			div $t0, $a1
			mfhi $t2
			bne $t2, $zero, printBoardAfterIf
			li $v0, 11
			li $a0, 10  # '\n'
			syscall
			li $a0, 124 # '|'
			syscall
		printBoardAfterIf:

		printBoardIf2:
			sll $t4, $t0, 2 #mult by 4
			add $t4, $t4, $t3
			lw $t4, 0($t4)
			beqz $t4, printSpace
			printPound:
				li $v0, 11
				li $a0, 35 # '#'
				syscall
				j printBoardAfterIf2
			printSpace:
				li $v0, 11
				li $a0, 32 # ' '
				syscall
		printBoardAfterIf2:
			li $a0, 124
			syscall
			
		addi $t0, $t0, 1
		j printBoardLoop
	printBoardAfterLoop:
	li $v0, 11
	li $a0, 10 # '\n''\n'
	syscall
	syscall
printBoardReturn:
jr $ra

doStep:
	# $t0 = i
	# $t1 = N*N
	# $t2 = destBoard
	# $t3 = N
	# $t4 = s
	li $t0, 0
	mult $a2, $a2
	mflo $t1
	move $t2, $a1
	move $t3, $a2

	
	doStepLoop:
		beq $t0, $t1, doStepAfterLoop
		addi $sp, $sp, -20
		sw $ra, 16($sp)
		sw $t0, 12($sp)
		sw $t1,  8($sp)
		sw $t2,  4($sp)
		sw $t3,  0($sp)
		
		move  $a1, $t3
		sll   $t5, $t0, 2
		add   $a2, $a0, $t5
		jal getNumNeighbors
		move  $t4, $v0
		
		lw $t3,  0($sp)
		lw $t2,  4($sp)
		lw $t1,  8($sp)
		lw $t0, 12($sp)
		lw $ra, 16($sp)
		addi $sp, $sp, 20

		doStepIf:
			sll $t5, $t0, 2
			add $t5, $t5, $a0
			lw  $t5, 0($t5)
			beqz $t5, doStepElseIf
				slti $t5, $t4, 2
				bne $zero, $t5, doStepIf2 # s < 2
				li $t5, 3
				slt $t5, $t5, $t4
				beqz $t5, doStepElse2 # 3 < s
			doStepIf2:
				sll $t5, $t0, 2
				add $t5, $t5, $t2
				sw  $zero, 0($t5)
				j doStepAfterIf2
			doStepElse2:
				sll $t5, $t0, 2
				add $t5, $t5, $t2
				li  $t6, 1
				sw  $t6, 0($t5)
			doStepAfterIf2:
			j doStepAfterIf

		doStepElseIf:
			bne $t4, 3, doStepElse
			sll $t5, $t0, 2
			add $t5, $t5, $t2
			li  $t6, 1
			sw  $t6, 0($t5)
			j doStepAfterIf
		doStepElse:
			sll $t5, $t0, 2
			add $t5, $t5, $t2
			sw  $zero, 0($t5)
		doStepAfterIf:
		addi $t0, $t0, 1
		j doStepLoop
	doStepAfterLoop:
doStepReturn:
jr $ra

getNumNeighbors:
	# $t0 = num
	# $t1 = col
	# $t2 = row
	# $t4 = N*(N-1)
	li   $t0, 0
	sub  $t3, $a2, $a0
	srl  $t3, $t3, 2
	div  $t3, $a1
	mfhi $t1
	mflo $t2
	mult $t2, $a1
	mflo $t2
	addi $t4, $a1, -1
	mult $t4, $a1
	mflo $t4

	GNNIf:
		blez $t1, GNNAfterIf
		GNNIf4:
			lw   $t3, -4($a2)
			beqz $t3, GNNAfterIf4
			addi $t0, $t0, 1
		GNNAfterIf4:
		GNNIf5:
			blez $t2, GNNAfterIf5
			sll  $t3, $a1, 2
			sub  $t3, $a2, $t3
			lw   $t3, -4($t3)
			beqz $t3, GNNAfterIf5
			addi $t0, $t0, 1
		GNNAfterIf5:
		GNNIf6:
			bge  $t2, $t4, GNNAfterIf6
			sll  $t3, $a1, 2
			add  $t3, $a2, $t3
			lw   $t3, -4($t3)
			beqz $t3, GNNAfterIf6
			addi $t0, $t0, 1
		GNNAfterIf6:
	GNNAfterIf:
	GNNIf2:
		add $t3, $a1, -1
		bge $t1, $t3, GNNAfterIf2
		GNNIf7:
			lw   $t3, 4($a2)
			beqz $t3, GNNAfterIf7
			addi $t0, $t0, 1
		GNNAfterIf7:
		GNNIf8:
			blez $t2, GNNAfterIf8
			sll  $t3, $a1, 2
			sub  $t3, $a2, $t3
			lw   $t3, 4($t3)
			beqz $t3, GNNAfterIf8
			addi $t0, $t0, 1
		GNNAfterIf8:
		GNNIf9:
			bge  $t2, $t4, GNNAfterIf9
			sll  $t3, $a1, 2
			add  $t3, $a2, $t3
			lw   $t3, 4($t3)
			beqz $t3, GNNAfterIf9
			addi $t0, $t0, 1
		GNNAfterIf9:
	GNNAfterIf2:
	GNNIf3:
		bge  $t2, $t4, GNNAfterIf3
		sll  $t3, $a1, 2
		add  $t3, $a2, $t3
		lw   $t3, ($t3)
		beqz $t3, GNNAfterIf3
		addi $t0, $t0, 1
	GNNAfterIf3:
	GNNIf10:
		blez $t2, GNNAfterIf10
		sll  $t3, $a1, 2
		sub  $t3, $a2, $t3
		lw   $t3, ($t3)
		beqz $t3, GNNAfterIf10
		addi $t0, $t0, 1
	GNNAfterIf10:
getNumNeighborsReturn:
move $v0, $t0
jr   $ra

setupBoard:
	# $t0 = selConf
	# $t1 = selConfSize
	# $t2 = selRowNum
	# $t3 = selColNum
	# $t4 = i
	# $t5 = t
	# $a0 = board
	move $t6, $a0
	setupBoardIf:
		bne $a2, 1, setupBoardElseIf
		li $v0, 4
		la $a0, selected1
		syscall
		la $t0, startConf1
		li $t1, 144
		li $t2, 9
		li $t3, 16
		j setupBoardAfterIf
	setupBoardElseIf:
		bne $a2, 3, setupBoardElse
		li $v0, 4
		la $a0, selected3
		syscall
		la $t0, startConf3
		li $t1, 21
		li $t2, 3
		li $t3, 7
		j setupBoardAfterIf
	setupBoardElse:
		li $v0, 4
		la $a0, selected2
		syscall
		la $t0, startConf2
		li $t1, 9
		li $t2, 3
		li $t3, 3
	setupBoardAfterIf:
	move $a0, $t6
	li $t4, 0
	setupBoardLoop:
		bge $t4, $a1, setupBoardAfterLoop
		bge $t4, $t2, setupBoardAfterLoop
		li  $t5, 0
		setupBoardLoop2:
			bge  $t5, $a1, setupBoardAfterLoop2
			bge  $t5, $t3, setupBoardAfterLoop2

			mult $t4, $t3
			mflo $t6
			add  $t6, $t6, $t5
			sll  $t6, $t6, 2
			add  $t6, $t6, $t0
			lw   $t6, ($t6)

			mult $t4, $a1
			mflo $t7
			add  $t7, $t7, $t5
			sll  $t7, $t7, 2
			add  $t7, $t7, $a0
			sw   $t6, ($t7)

			addi $t5, $t5, 1
			j setupBoardLoop2
		setupBoardAfterLoop2:
		addi $t4, $t4, 1
		j setupBoardLoop	
	setupBoardAfterLoop:
setupBoardReturn:
jr $ra
	

.data
name:       .asciiz "Game of Life by CD Silsby\n"
prompt1:    .asciiz "Please enter the size of the board\n"
prompt2:    .asciiz "Please select a starting configuration\n"
option1:    .asciiz "1  Pentadecathlon\n"
option2:    .asciiz "2  R-pentomino\n"
option3:    .asciiz "3  Acorn\n"
selected1:  .asciiz "Pentadecathlon selected\n"
selected2:  .asciiz "R-pentomino selected\n"
selected3:  .asciiz "Acorn selected\n"
continue:   .asciiz "Press the ENTER key to continue\n"
lf:         .asciiz "\n"

board1:     
            .align 2
            .space 10000 #supports board sizes up to 100
board2:     
            .align 2
            .space 10000
			

startConf1: .word 0, 0, 0, 0, 0, 0, 0, 0, 0
            .word 0, 0, 0, 0, 0, 0, 0, 0, 0
            .word 0, 0, 0, 0, 0, 0, 0, 0, 0
            .word 0, 0, 0, 0, 1, 0, 0, 0, 0
            .word 0, 0, 0, 1, 0, 1, 0, 0, 0
            .word 0, 0, 1, 0, 0, 0, 1, 0, 0
            .word 0, 0, 1, 0, 0, 0, 1, 0, 0
            .word 0, 0, 1, 0, 0, 0, 1, 0, 0
            .word 0, 0, 1, 0, 0, 0, 1, 0, 0
            .word 0, 0, 1, 0, 0, 0, 1, 0, 0
            .word 0, 0, 1, 0, 0, 0, 1, 0, 0
            .word 0, 0, 0, 1, 0, 1, 0, 0, 0
            .word 0, 0, 0, 0, 1, 0, 0, 0, 0
            .word 0, 0, 0, 0, 0, 0, 0, 0, 0
            .word 0, 0, 0, 0, 0, 0, 0, 0, 0
            .word 0, 0, 0, 0, 0, 0, 0, 0, 0

startConf2: .word 0, 1, 1
            .word 1, 1, 0
            .word 0, 1, 0

startConf3:
            .word 0, 1, 0, 0, 0, 0, 0
            .word 0, 0, 0, 1, 0, 0, 0
            .word 1, 1, 0, 0, 1, 1, 1
